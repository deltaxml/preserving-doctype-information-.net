# Preserving Doctype Information

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML Compare release. The resources should be located such that they are two levels below the top level release directory.*

*For example `DeltaXML-XML-Compare-10_0_0_n/samples/preserving-doctype-information`.*

---

How to include doctype information in the result file that is determined by the input file Doctype rather than hard-coded into the pipeline.

This document describes how to run the sample. For concept details see: [Preserving Doctype Information](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/preserving-doctype-information)

## Running the Sample
The sample can be run via a *run.bat* batch file, so long as this is issued from the sample directory.

# **Note - .NET support has been deprecated as of version 10.0.0 **